package com.ostapiuk.controller;

import com.ostapiuk.model.ElectricalApplianceManager;
import com.ostapiuk.view.UserInterface;

public class ElectricalApplianceController {
    private ElectricalApplianceManager manager;
    private UserInterface view;

    public ElectricalApplianceController(UserInterface userInterface) {
        manager = new ElectricalApplianceManager();
        view = userInterface;
    }

    public final void getApplianceList() {
        manager.applianceList = manager.getApplianceList();
        updateView();
    }

    public final void sortByModel() {
        manager.applianceList = manager.sortByModel();
        updateView();
    }

    public final void sortByProducingCountry() {
        manager.applianceList = manager.sortByProducingCountry();
        updateView();
    }

    public final void sortByPower() {
        manager.applianceList = manager.sortByPower();
        updateView();
    }

    public final void sortByPrice() {
        manager.applianceList = manager.sortByPrice();
        updateView();
    }

    public final void findByModel(String model) {
        manager.applianceList = manager.findByModel(model);
        updateView();
    }

    public final void findByProducingCountry(String country) {
        manager.applianceList = manager.findByProducingCountry(country);
        updateView();
    }

    public final void findByPower(int power) {
        manager.applianceList = manager.findByPower(power);
        updateView();
    }

    public final void findByPrice(int price) {
        manager.applianceList = manager.findByPrice(price);
        updateView();
    }

    public final void updateView() {
        view.printResult(manager.applianceList);
    }
}
