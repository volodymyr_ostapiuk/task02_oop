package com.ostapiuk.model.entity;

public class Microwave extends ElectricalAppliance {
    private boolean isClock;

    public Microwave(final String model, final String producingCountry,
                     final int power, final boolean clock) {
        super(model, producingCountry, power);
        isClock = clock;
    }

    public final boolean isClock() {
        return isClock;
    }

    @Override
    public final String toString() {
        return "Microwave's" + super.toString()
                + ", clock is " + isClock;
    }
}
