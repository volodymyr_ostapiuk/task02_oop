package com.ostapiuk.model.entity;

public class Stove extends ElectricalAppliance {
    private int stoveNumberOfBurners;

    public Stove(final String model, final String producingCountry,
                 final  int power, final int numberOfBurners) {
        super(model, producingCountry, power);
        stoveNumberOfBurners = numberOfBurners;
    }

    public final int getStoveNumberOfBurners() {
        return stoveNumberOfBurners;
    }

    @Override
    public final int calculateAppliancePrice() {
        return super.calculateAppliancePrice() * stoveNumberOfBurners;
    }

    @Override
    public final String toString() {
        return "Stove's" + super.toString()
                + ", number of burners is " + stoveNumberOfBurners;
    }
}
