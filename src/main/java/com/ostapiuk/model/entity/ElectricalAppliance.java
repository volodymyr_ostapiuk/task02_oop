package com.ostapiuk.model.entity;

public abstract class ElectricalAppliance {
    private String applianceModel;
    private String applianceProducingCountry;
    private int appliancePower;
    private int appliancePrice;

    public ElectricalAppliance(final String model, final String
            producingCountry, final int power) {
        applianceModel = model;
        applianceProducingCountry = producingCountry;
        appliancePower = power;
        appliancePrice = getAppliancePower();
    }

    public final String getApplianceModel() {
        return applianceModel;
    }

    public final String getApplianceProducingCountry() {
        return applianceProducingCountry;
    }

    public final int getAppliancePower() {
        return appliancePower;
    }

    public int getAppliancePrice() {
        return calculateAppliancePrice();
    }

    public int calculateAppliancePrice() {
        final int applianceTax = 1000;
        return appliancePower * applianceTax;
    }

    @Override
    public String toString() {
        return " model is " + applianceModel
                + ", producing country is " + applianceProducingCountry
                + ", power is " + appliancePower
                + ", price is " + appliancePrice;
    }
}
