package com.ostapiuk.model.entity;

public class Fridge extends ElectricalAppliance {
    private boolean isFreezer;

    public Fridge(final String model, final String producingCountry,
                  final int power, final boolean freezer) {
        super(model, producingCountry, power);
        isFreezer = freezer;
    }

    public final boolean isFreezer() {
        return isFreezer;
    }

    @Override
    public final int calculateAppliancePrice() {
        if (isFreezer) {
            final int freezerPrice = 200;
            return super.calculateAppliancePrice() + freezerPrice;
        } else {
            return super.calculateAppliancePrice();
        }
    }

    @Override
    public final String toString() {
        return "Fridge's " + super.toString()
                + ", freezer is " + isFreezer;
    }
}
