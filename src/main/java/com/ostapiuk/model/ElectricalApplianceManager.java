package com.ostapiuk.model;

import com.ostapiuk.model.creator.ElectricalApplianceCreator;
import com.ostapiuk.model.entity.ElectricalAppliance;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ElectricalApplianceManager {
    private ElectricalApplianceCreator applianceCreator;
    public List<ElectricalAppliance> applianceList;

    public ElectricalApplianceManager() {
        applianceCreator = new ElectricalApplianceCreator();
        applianceList = new ArrayList<>();
    }

    public final List<ElectricalAppliance> getApplianceList() {
        return applianceCreator.getElectricalApplianceList();
    }

    public final List<ElectricalAppliance> findByModel(
            final String model) {
        applianceList = getApplianceList().stream()
                .filter(appliance -> appliance.getApplianceModel()
                        .equals(model)).sorted(Comparator.comparing(ElectricalAppliance::
                        getApplianceModel)).collect(Collectors.toList());
        return applianceList;
    }

    public final List<ElectricalAppliance> findByProducingCountry(
            final String producingCountry) {
        applianceList = getApplianceList().stream()
                .filter(appliance -> appliance.getApplianceProducingCountry()
                        .equals(producingCountry)).sorted(Comparator.comparing(ElectricalAppliance::
                        getApplianceProducingCountry)).collect(Collectors.toList());
        return applianceList;
    }

    public final List<ElectricalAppliance> findByPower(
            final int power) {
        applianceList = getApplianceList().stream()
                .filter(appliance -> appliance.getAppliancePower() >= power)
                .sorted(Comparator.comparingInt(ElectricalAppliance::
                        getAppliancePower)).collect(Collectors.toList());
        return applianceList;
    }

    public final List<ElectricalAppliance> findByPrice(
            final int price) {
        applianceList = getApplianceList().stream()
                .filter(appliance -> appliance.getAppliancePrice() >= price)
                .sorted(Comparator.comparingInt(ElectricalAppliance::
                        getAppliancePrice)).collect(Collectors.toList());
        return applianceList;
    }

    public final List<ElectricalAppliance> sortByProducingCountry() {
        applianceList = getApplianceList();
        applianceList.sort(Comparator.comparing
                (ElectricalAppliance::getApplianceProducingCountry));
        return applianceList;
    }

    public final List<ElectricalAppliance> sortByModel() {
        applianceList = getApplianceList();
        applianceList.sort(Comparator.comparing
                (ElectricalAppliance::getApplianceModel));
        return applianceList;
    }

    public final List<ElectricalAppliance> sortByPower() {
        applianceList = getApplianceList();
        applianceList.sort(Comparator.comparingInt
                (ElectricalAppliance::getAppliancePower));
        return applianceList;
    }

    public final List<ElectricalAppliance> sortByPrice() {
        applianceList = getApplianceList();
        applianceList.sort(Comparator.comparingInt
                (ElectricalAppliance::getAppliancePrice));
        return applianceList;
    }
}
