package com.ostapiuk.model.creator;

import com.ostapiuk.model.entity.ElectricalAppliance;
import com.ostapiuk.model.entity.Fridge;
import com.ostapiuk.model.entity.Microwave;
import com.ostapiuk.model.entity.Stove;

import java.util.ArrayList;
import java.util.List;

public class ElectricalApplianceCreator {
    private List<ElectricalAppliance> electricalApplianceList;

    public ElectricalApplianceCreator() {
        electricalApplianceList = createApplianceList();
    }

    public final List<ElectricalAppliance> getElectricalApplianceList() {
        return electricalApplianceList;
    }

    private List<ElectricalAppliance> createApplianceList() {
        electricalApplianceList = new ArrayList<>();
        electricalApplianceList.add(new Microwave("Samsung ME3",
                "China", 45, true));
        electricalApplianceList.add(new Microwave("Sharp R200",
                "Germany", 60, false));
        electricalApplianceList.add(new Microwave("Mystery MMV",
                "USA", 52, false));
        electricalApplianceList.add(new Microwave("LG MS2",
                "Japan", 37, true));
        electricalApplianceList.add(new Stove("Vilgrand VGS",
                "Austria", 81, 2));
        electricalApplianceList.add(new Stove("Elna 01-P",
                "Ukraine", 70, 4));
        electricalApplianceList.add(new Stove("Gefest 802",
                "Greece", 100, 1));
        electricalApplianceList.add(new Fridge("Delfa F-50",
                "France", 94, true));
        electricalApplianceList.add(new Fridge("Atlant 80-O",
                "Belarus", 66, false));
        electricalApplianceList.add(new Fridge("Elenberg 1304",
                "Sweden", 88, false));
        electricalApplianceList.add(new Fridge("NANSA FM",
                "Canada", 105, true));
        return electricalApplianceList;
    }
}
