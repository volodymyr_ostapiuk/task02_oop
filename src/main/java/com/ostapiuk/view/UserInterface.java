package com.ostapiuk.view;

import com.ostapiuk.controller.ElectricalApplianceController;
import com.ostapiuk.model.entity.ElectricalAppliance;

import java.util.List;
import java.util.Scanner;

public class UserInterface {
    private ElectricalApplianceController controller;

    public UserInterface() {
        controller = new ElectricalApplianceController(this);
    }

    private final void showMenu() {
        System.out.println("1 - View all appliance");
        System.out.println("2 - Sort appliance by parameter");
        System.out.println("3 - Find appliance by parameter");
        System.out.println("4 - For exit");
        System.out.print("Press number from 1 to 4: ");
    }

    public final void getUserChoice() {
        boolean check = false;
        Scanner userInput = new Scanner(System.in);
        do {
            showMenu();
            int choice = userInput.nextInt();
            switch (choice) {
                case 1:
                    controller.getApplianceList();
                    break;
                case 2:
                    getUserSortChoice(userInput);
                    break;
                case 3:
                    getUserFindChoice(userInput);
                    break;
                case 4:
                    check = true;
                    break;
                default:
                    break;
            }
        } while (!check);
    }

    private final void showSortMenu() {
        System.out.println("1 - Sort by model");
        System.out.println("2 - Sort by producing country");
        System.out.println("3 - Sort by power");
        System.out.println("4 - Sort by price");
        System.out.print("Press number from 1 to 4: ");
    }

    private final void getUserSortChoice(Scanner scanner) {
        showSortMenu();
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                controller.sortByModel();
                break;
            case 2:
                controller.sortByProducingCountry();
                break;
            case 3:
                controller.sortByPower();
                break;
            case 4:
                controller.sortByPrice();
                break;
            default:
                break;
        }
    }

    private final void showFindMenu() {
        System.out.println("1 - Find by model");
        System.out.println("2 - Find by producing country");
        System.out.println("3 - Find by power");
        System.out.println("4 - Find by price");
        System.out.print("Press number from 1 to 4: ");
    }

    private final void getUserFindChoice(Scanner scanner) {
        showFindMenu();
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                controller.findByModel(getFindStringChoice(scanner));
                break;
            case 2:
                controller.findByProducingCountry(getFindStringChoice(scanner));
                break;
            case 3:
                controller.findByPower(getFindIntChoice(scanner));
                break;
            case 4:
                controller.findByPrice(getFindIntChoice(scanner));
                break;
            default:
                break;
        }
    }

    private final String getFindStringChoice(Scanner userInput) {
        System.out.print("Press find word: ");
        String choice = userInput.next();
        return choice;
    }

    private final int getFindIntChoice(Scanner userInput) {
        System.out.print("Press find number: ");
        int choice = userInput.nextInt();
        return choice;
    }

    public final void printResult(List<ElectricalAppliance> appliances) {
        for (ElectricalAppliance appliance : appliances) {
            System.out.println(appliance);
        }
    }
}
